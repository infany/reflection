﻿using CloudWorks.Store.BaseClasses;
using CloudWorks.Store.Classes;
using CloudWorks.Store.Interfaces;
using CloudWorks.Store.Providers;
using CloudWorks.Store.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CloudWorks.Store
{
    internal class Program
    {

        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            Console.InputEncoding = System.Text.Encoding.Unicode;

            int seldCheckoutCount = 0;
            while (seldCheckoutCount == 0)
            {
                Console.WriteLine("Введіть загальну кількість кас самообслуговування в магазині");
                int.TryParse(Console.ReadLine(), out seldCheckoutCount);
            }

            /* Filling Persons */
            Person[] persons = null;

            int iteration = 10000000;

            Action directCall = () =>
            {
                var _ = new StaticPersonProvider().GetPersons();
            };

            Action reflectionCall = () =>
            {
                var mi = typeof(StaticPersonProvider).GetMethod("GetPersons",
                    System.Reflection.BindingFlags.Instance
                   | System.Reflection.BindingFlags.Public);

                var _ = mi.Invoke(new StaticPersonProvider(), null);
            };

            Console.WriteLine("Direction: {0}",
            TimingMeasuringHelper.Measuring(directCall, iteration));

            Console.WriteLine("Relection: {0}",
            TimingMeasuringHelper.Measuring(reflectionCall, iteration));

            //uncomment to use console person provider.
            persons = new ConsolePersonProvider().GetPersons();

            /*// uncomment to make example file.
            IPersonProvider pp = new StaticPersonProvider();
            persons = pp.GetPersons();
            fsp.SavePersons(persons);*/

            /*// File PersonProvider.
            var fsp = new FilePersonProvider("new.json");
            persons = fsp.GetPersons();
            */

            /*******************/

            var store = new Classes.Store(seldCheckoutCount, persons);

            foreach (var person in persons)
            {
                person.Print();
            }

            store.Print();

            Console.WriteLine($"Загальний час обслуговування: {store.Checkout()}");
        }

    }
}
