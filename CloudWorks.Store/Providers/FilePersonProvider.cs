﻿using CloudWorks.Store
    .BaseClasses;
using CloudWorks.Store.Classes;
using CloudWorks.Store.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CloudWorks.Store.Providers
{
    public class FilePersonProvider : IPersonProvider
    {
        private readonly string fileName;

        public FilePersonProvider(string fileName)
        {
            if (fileName is null)
            {
                throw new ArgumentNullException(nameof(fileName));
            }

            this.fileName = fileName;
        }

        public Person[] GetPersons()
        {
            var dto = JsonSerializer.Deserialize<DTO>(File.ReadAllText(fileName));
            var list = new List<Person>(dto.Clients);
            list.AddRange(dto.Employees);
            return list.ToArray();
        }

        public void SavePersons(Person[] persons)
        {
            var dto = new DTO
            {
                Employees = persons.Where(p => p is Employee).Cast<Employee>().ToArray(),
                Clients = persons.Where(p => p is Client).Cast<Client>().ToArray(),
            };
            File.WriteAllText(fileName, JsonSerializer.Serialize<DTO>(dto));
        }

        public class DTO
        {
            public Employee[] Employees { get; set; }
            public Client[] Clients { get; set; }
        }

    }
}
